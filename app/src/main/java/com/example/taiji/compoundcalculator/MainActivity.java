package com.example.taiji.compoundcalculator;

import android.content.Intent;
import android.icu.math.BigDecimal;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn = (Button) findViewById(R.id.button);
        calculate(btn);

        Button nextButton = (Button)findViewById(R.id.button2);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
Intent intent = new Intent(MainActivity.this, ImageButtonActivity.class);
startActivity(intent);
            }
        });

        Button layoutButton = (Button)findViewById(R.id.button3);
        layoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LayoutActivity.class);
                startActivity(intent);
            }
        });
    }

    private void calculate(Button btn) {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText principalText = (EditText)findViewById(R.id.principal);
                principalText.selectAll();
                double principal = 0;
                principal = Integer.parseInt(principalText.getText().toString());


                EditText mounthText = (EditText)findViewById(R.id.mounth);
                mounthText.selectAll();
                int mounth = 0;
                mounth = Integer.parseInt(mounthText.getText().toString());


                EditText yeildText = (EditText)findViewById(R.id.yield);
                yeildText.selectAll();
                double yield = 0;
                yield = Integer.parseInt(yeildText.getText().toString());
yield = yield * 0.01 + 1;
                double ans = 0;
                double aa = Math.pow(yield,mounth);
                ans = principal * aa;
                double anwews = Math.floor (ans);


                TextView tv = (TextView)findViewById(R.id.textView3);
                String answer = String.valueOf(anwews);
                tv.setText(answer);
            }
        });
    }
}
